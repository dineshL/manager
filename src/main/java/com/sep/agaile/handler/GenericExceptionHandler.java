package com.sep.agaile.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Component
public class GenericExceptionHandler implements HandlerExceptionResolver {

	@Override
	public ModelAndView resolveException(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3) {
		ModelAndView mv = new ModelAndView("exceptions/generic");
		mv.addObject("message", arg3.getMessage());
		return mv;
	}

}
