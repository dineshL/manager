package com.sep.agaile.validators;

import org.sep.agile.model.dto.UserCredential;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Component
public class CredentialValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return UserCredential.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object object, Errors errors) {
		UserCredential c = (UserCredential) object;
		if(c.getPassword().trim().length() < 6) {
			errors.rejectValue("password", "too short", "Password must contain at least six characters!");
		}
		
		if(c.getUsername().equals(c.getUsername())) {
			errors.rejectValue("password", "equal username & password", "Password must be different from Username!");
		}
	}

}
