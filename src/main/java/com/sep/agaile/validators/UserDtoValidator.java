package com.sep.agaile.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sep.agile.model.dto.UserDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Component
public class UserDtoValidator implements Validator{

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"  
			   + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";  
			 String ID_PATTERN = "[0-9]+";  
			 String STRING_PATTERN = "[a-zA-Z]+";  
			 String MOBILE_PATTERN = "[0-9]{10}";
			 
	private Pattern pattern;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return UserDTO.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object obejct, Errors err) {
		UserDTO dto = (UserDTO) obejct;
		
		if(dto.getUsername().trim().length() < 1) {
			err.rejectValue("username", "invalid name" ,"Please check the name again");
		}
		
		if(dto.getPassword().trim().length() < 8) {
			err.rejectValue("password", "invalid password", "Password should be 8 charcters long");
		}
		
		Matcher matcher;
		if(dto.getEmail().trim().length() < 1) {
			err.rejectValue("email", "invalid email", "Email address is compalsary");
		}else {
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(dto.getEmail());
			
			if(!matcher.matches()) {
				err.rejectValue("email", "invalid email", "Please enter a valid email address");
			}
		}
		
	}

}
