package com.sep.agaile.data;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.jackson.JacksonFeature;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class CallManager {

		public static WebTarget getRootTarget() {
			WebTarget rootTarget = null;
			try{
				final Client client = ClientBuilder.newBuilder()
						.register(JacksonFeature.class)
						.build();
				
				
				rootTarget = client.target("http://localhost:8080/server/rest");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return rootTarget;
		} //end of getClient;
		
		
}
