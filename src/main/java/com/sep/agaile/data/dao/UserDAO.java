package com.sep.agaile.data.dao;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.sep.agile.model.dto.Message;
import org.sep.agile.model.dto.UserDTO;

import com.sep.agaile.data.CallManager;
import com.sep.agaile.exceptions.ServerSideException;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class UserDAO {

private WebTarget baseTarget = null;
	
	public UserDAO() {
		baseTarget = CallManager.getRootTarget().path("users");
	}
	
	/**
	 * Send UserDTO to the serverside
	 * @param UserDTO 
	 * @return
	 */
	public UserDTO insertUser(UserDTO dto) {
		
		Response response = baseTarget
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(dto, MediaType.APPLICATION_JSON));
		
		if(response.getStatus() != Status.OK.getStatusCode()) {
			throw new ServerSideException((Message) response.readEntity(Message.class));
			
		}
				
		return (UserDTO) response.readEntity(UserDTO.class);
		
	}
	
	/**
	 * get a user filtered using the username of the user
	 * @param String username
	 * @return UserDTO object
	 */
	public UserDTO getUser(String username) {
		
		Response response = baseTarget
				.path(username)
				.request(MediaType.APPLICATION_JSON)
				.get();
		
		if(response.getStatus() != Status.OK.getStatusCode()) {
			throw new ServerSideException((Message) response.readEntity(Message.class));
			
		}
		
		return (UserDTO) response.readEntity(UserDTO.class);
	}
	
	public boolean updateUser(UserDTO dto) {
		
		Response response = baseTarget
				.path(dto.getUsername())
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(dto, MediaType.APPLICATION_JSON));
		
		if(response.getStatus() != Status.OK.getStatusCode()) {
			throw new ServerSideException((Message) response.readEntity(Message.class));
			
		}
		
		return true;
	}
	
	
	/**
	 * Activate a particular user using the code provided in the email
	 * @param code
	 * @return
	 */
	public Long activateUser(String code) {
		
		Response response = baseTarget.path("activate")
				.queryParam("code", code)
				.request(MediaType.APPLICATION_JSON)
				.get();
		
		if(response.getStatus() != Status.OK.getStatusCode()) {
			throw new ServerSideException((Message) response.readEntity(Message.class));
			
		}
		
		return Long.parseLong(response.readEntity(Message.class).getMessage());
	}
}
