package com.sep.agaile.data.dao;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.sep.agile.model.dto.Message;
import org.sep.agile.model.dto.ProjectDTO;

import com.sep.agaile.data.CallManager;
import com.sep.agaile.exceptions.ServerSideException;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class ProjectDAO {

	/** Web service callings for the project procedures **/
	
	private WebTarget baseTarget = null;

	public ProjectDAO() {
		baseTarget = CallManager.getRootTarget().path("projects");
	}

	/**
	 * Add a new project to the database
	 * @param ProjectDTO object
	 * @return boolean value indicating the addition of the dto object
	 */
	public boolean addNewProject(ProjectDTO dto) {
		Response response = baseTarget
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(dto, MediaType.APPLICATION_JSON));
		
		if(response.getStatus() != Status.CREATED.getStatusCode()) {
			throw new ServerSideException((Message) response.readEntity(Message.class));
		}
		
		return true;
	}

}
