package com.sep.agaile.data.dao;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.sep.agaile.data.CallManager;
import com.sep.agile.lists.UserStoryList;

public class UserStoryDAO {

	private WebTarget baseTarget = null;
	
	public UserStoryDAO() {
		
		baseTarget = CallManager.getRootTarget().path("userstories");
	}
	
	public UserStoryList getToDoList() {
		
		System.out.print("this is client side userstorydao for gettodo list");
		Response response = baseTarget.request(MediaType.APPLICATION_JSON).get();
		
		if(response.getStatus() != 200) {
			return null;
		}
		
		return (UserStoryList) response.readEntity(UserStoryList.class);
	
	}
	
	public boolean deleteUserStoryId(String id ) {
		
		boolean result = false;
		System.out.println("id is "+id);
		Response response = baseTarget.path("deleteid").request(MediaType.APPLICATION_JSON).post(Entity.entity(id, MediaType.APPLICATION_JSON));
		
		if(response.getStatus() != 200) {
			System.out.println("ok");
			result = false;
		}
		
		System.out.println("ok ok");
		return result= true;
		
	}
}
