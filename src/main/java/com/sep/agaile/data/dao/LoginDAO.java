package com.sep.agaile.data.dao;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.sep.agile.model.dto.Message;
import org.sep.agile.model.dto.UserCredential;
import org.sep.agile.model.dto.UserDTO;

import com.sep.agaile.data.CallManager;
import com.sep.agaile.exceptions.ServerSideException;

/**
 * Version 1.0 
 * @author Dinesh Liyanage
 *
 */
public class LoginDAO {
	
	/** Web service callings for the login logout procedures **/

	private WebTarget baseTarget = null;
	
	public LoginDAO() {
		baseTarget = CallManager.getRootTarget().path("users").path("authenticate");
	}
	
	
	/**
	 * Login procedure
	 * @param UserCredential object
	 * @return UserDto object
	 * @throws ServerSideException
	 */
	public UserDTO login(UserCredential credential) {
		
		Response response = baseTarget.path("login")
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(credential, MediaType.APPLICATION_JSON));
		
		if(response.getStatus() != Status.OK.getStatusCode()) {
			throw new ServerSideException((Message) response.readEntity(Message.class));
			
		}
		
		UserDTO dto = (UserDTO) response.readEntity(UserDTO.class);
		
		return dto;
		
	}
		
}
