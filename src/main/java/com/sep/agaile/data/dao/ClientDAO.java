package com.sep.agaile.data.dao;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.sep.agile.model.dto.ClientDTO;
import org.sep.agile.model.dto.Message;
import org.sep.agile.model.dto.lists.EmailList;

import com.sep.agaile.data.CallManager;
import com.sep.agaile.exceptions.ServerSideException;
/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class ClientDAO {

private static final String ClientDTO = null;
private WebTarget baseTarget = null;
	/** web service callings for the client **/

	public ClientDAO() {
		baseTarget = CallManager.getRootTarget().path("clients");
	}

	/**
	 * Add a new client 
	 * @param ClientDTO object
	 * @return boolean value indicating whether the client added or not
	 */
	public boolean addNewClient(ClientDTO dto) {
		
		Response response = baseTarget
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(dto, MediaType.APPLICATION_JSON));
		
		if(response.getStatus() != Status.CREATED.getStatusCode()) {
			throw new ServerSideException((Message) response.readEntity(Message.class));
			
		}
		
		return true;
	}
	
	/**
	 * Get Emils of the clients
	 * @return EmailList object
	 */
	public EmailList getClientEmails() {
		
		Response response = baseTarget
				.queryParam("get", "email")
				.request(MediaType.APPLICATION_JSON)
				.get();
		
		if(response.getStatus() != Status.OK.getStatusCode()) {
			throw new ServerSideException((Message) response.readEntity(Message.class));
			
		}
		
		EmailList list = (EmailList) response.readEntity(EmailList.class);
		
		return list;
	}
	
	
	/**
	 * Find client by the client id
	 * @param Long client id
	 * @return ClientDTO object
	 */
	public ClientDTO findById(long id) {
		Response response = baseTarget.path(String.valueOf(id))
				.request(MediaType.APPLICATION_JSON)
				.get();
		
		//if(response.getStatus() != Status.OK.getStatusCode()) {
			
			//throw new ServerSideException((Message) response.readEntity(Message.class));
		//}
		ClientDTO dto = new ClientDTO();
		/*try {
		 dto = (ClientDTO) response.readEntity(ClientDTO.class);
		}catch(Exception e) {
			e.printStackTrace();
		}*/
		
		dto.setName("Client name");
		
		return dto;
	}
}
