package com.sep.agaile.exceptions;

import org.sep.agile.model.dto.Message;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
public class ServerSideException extends RuntimeException {

	/** Represents a servrside exception **/
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServerSideException(Message message) {
		super(formatMessage(message.getMessage()));
	}
	
	private static String formatMessage(String message) {
		if(message.contains("for")) {
			message = message.substring(0, message.indexOf("for"));
		}
		
		return message;
	}
}
