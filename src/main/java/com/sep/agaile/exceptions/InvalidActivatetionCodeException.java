package com.sep.agaile.exceptions;

/**
 * 
 * @author Dinesh Liyanage
 *
 */
public class InvalidActivatetionCodeException extends RuntimeException{

	
	/**
	 *  Thrown when a invalid activation code is provided
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidActivatetionCodeException(String message) {
		super(message);
	}
}
