package com.sep.agaile.exceptions;

/**
 * 
 * @author Dinesh Liyanage
 *
 */
public class UnauthorizedAction extends RuntimeException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnauthorizedAction(String message) {
		super(message);
	}
}
