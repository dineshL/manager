package com.sep.agaile.controllers;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.sep.agaile.data.dao.UserStoryDAO;
import com.sep.agile.lists.UserStoryList;

/**
 * version 1.0
 * @author Dhananjaya Subhash
 */
@Controller
@RequestMapping(value="/userstories")
public class UserStoryController {

    /**
     * URL: /rest/controller/view
     * viewMyWork() : view my work page
     * @param model : Model auto passed
     * @return ModelAndView
     */
	@RequestMapping(params="view" , method = RequestMethod.GET)
	public ModelAndView viewMyWork(Model model) {
		System.out.println("view my work controller methode was called");
		ModelAndView modelandview = new ModelAndView("workByPerson");		
		UserStoryList userlistdto = new UserStoryDAO().getToDoList();		
		modelandview.addObject("dto",userlistdto);
		
		return modelandview;		
	}

    /**
     * URL: /rest/controller/delete
     * deleteUserStory(): delete user stories in my work page
     * @param request : HttpServletRequest auto passed
     * @return String
     */
   @RequestMapping(params="delete",method = RequestMethod.POST)
    public String deleteUserStory(HttpServletRequest request) {	   
    	String id = request.getParameter("delete");
    	System.out.println(id);
    	UserStoryDAO dao = new UserStoryDAO();
    	dao.deleteUserStoryId(id);
    	
    	return "userstory/mywork";
    }
}
