package com.sep.agaile.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.sep.agile.model.dto.ClientDTO;
import org.sep.agile.model.dto.ProjectDTO;
import org.sep.agile.model.dto.UserCredential;
import org.sep.agile.model.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sep.agaile.data.dao.ClientDAO;
import com.sep.agaile.data.dao.UserDAO;
import com.sep.agaile.exceptions.InvalidActivatetionCodeException;
import com.sep.agaile.exceptions.ServerSideException;
import com.sep.agaile.exceptions.UnauthorizedAction;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */

@Controller
@RequestMapping(value = "/client")
public class ClientController {
	/** Controls the Client related activities **/

	private static final Logger logger = LoggerFactory.getLogger(ClientController.class);

	/**
	 * Load basic client view to the main frame
	 * @param 
	 * @return
	 */
	@RequestMapping(params = "add", method = RequestMethod.GET)
	public String loadClientAddView(Model model) {

		logger.info("Loading add new client view");

		model.addAttribute("clientDto", new ClientDTO());
		return "client/add";

	}

	/**
	 * Insert a nre client to the databse
	 * @param ClientDTO bound to the jsp form
	 * @return String
	 */
	@RequestMapping(params = "add", method = RequestMethod.POST)
	public String saveNewClient(@ModelAttribute("clientDto") ClientDTO dto, Model model, HttpSession session) {

		logger.info("Starting to add a new client");

		UserDTO currentUser = ((UserDTO) session.getAttribute("user"));

		if(currentUser.getRole().equals("Project Owner") || currentUser.getRole().equals("Project Manager")) { // checking user privilege

			ClientDAO dao = new ClientDAO();
			dao.addNewClient(dto);

			model.addAttribute("project", new ProjectDTO());
			return "user/home";

		}else {

			throw new UnauthorizedAction("You are not allowed to preform this task");
		}
	}

	/**
	 * Load email verification page of the client
	 * @return String (location of the jsp file)
	 */
	@RequestMapping(method = RequestMethod.GET, params = "verify")
	public String loadEmailVerficationPage(@RequestParam("code") String code) {
		UserDAO dao = new UserDAO();
		//String code = (String)req.getParameter("code");
		logger.info("activation code is: " + code);
		Long activateUserId = dao.activateUser(code);
		if(activateUserId != -1) {
			return "redirect:/client/register?id=" + activateUserId;
		}else {
			throw new InvalidActivatetionCodeException("Activation code seems to be invalid, please try again.<br> if this message comes again and again contact support");
		}
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/register")
	public String registerClient(@RequestParam("id") Long userId, Model model) {
		logger.info("loadin clien register  for client: " + userId);
		
		ClientDAO dao = new ClientDAO();
		
		model.addAttribute("userDto", dao.findById(userId));
		return "client/register";
	}
	
	@RequestMapping(params = "update", method = RequestMethod.POST)
	public String updateClientRegistration(Model model) {
		
		model.addAttribute("userDto", new UserDTO());
		model.addAttribute("credential", new UserCredential());
		logger.info("client update calling");
		return "home";
	}



	/***********************************************************************
	 *                     Exception Handlers                              *
	 ***********************************************************************/

	/**
	 * 
	 * Called when a server error is occuared and diaplay's the error message to the user
	 * @param ServerSideException
	 * @return
	 */
	@ExceptionHandler(ServerSideException.class)
	public ModelAndView serverSideExceptionHandler(HttpServletRequest request, Exception exception) {

		logger.error("Request " + request.getRequestURI() + " raised an exception " + exception);

		ModelAndView mv = new ModelAndView("exceptions/server_exception");
		mv.addObject("message", exception.getMessage());
		return mv;
	}
	
	@ExceptionHandler(InvalidActivatetionCodeException.class)
	public ModelAndView InvalidActivatetionCodeExceptionHandler(HttpServletRequest request, Exception exception) {

		logger.error("Request " + request.getRequestURI() + " raised an exception " + exception);

		ModelAndView mv = new ModelAndView("exceptions/server_exception");
		mv.addObject("message", exception.getMessage());
		return mv;
	}


}
