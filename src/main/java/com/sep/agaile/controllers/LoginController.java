package com.sep.agaile.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.sep.agile.model.dto.ProjectDTO;
import org.sep.agile.model.dto.UserCredential;
import org.sep.agile.model.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sep.agaile.data.dao.ClientDAO;
import com.sep.agaile.data.dao.LoginDAO;
import com.sep.agaile.exceptions.ServerSideException;
import com.sep.agaile.validators.CredentialValidator;


/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Controller
public class LoginController {
	
	@Autowired
	private CredentialValidator credentialValidator;
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	/**
	 * 
	 * @param Credential bound with the login view containing the username and the password
	 * @return 
	 */
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public ModelAndView verifyLogin(@ModelAttribute("credential") UserCredential credential,
			BindingResult bindingResult,
			HttpSession session, 
			Model model) {
		
		LoginDAO dao = new LoginDAO(); 			//creating the login dao object;
		
		UserDTO dto = dao.login(credential); 	//calling the login method and getting the dt object {here it should handle the exception} 
		
		if(dto == null) { 						// the provided user credentials are incorrect
			credential.setPassword(null);
			
			bindingResult.rejectValue("password", "invalid credentials", "Invalid username or password");
			model.addAttribute("userDto", new UserDTO());
			model.addAttribute("credential", credential);
			
			ModelAndView mv = new ModelAndView("home", bindingResult.getModel());
			return mv; 							// redirect to the same interface and dsplay error messages
		}
		
		
		session.setAttribute("user", dto);
		ModelAndView mv = new ModelAndView("user/home");
		
		mv.addObject("project", new ProjectDTO());
		mv.addObject("clientEmails", new ClientDAO().getClientEmails());
		return mv;  								// redirect to the home interface of the logged user
	}
	
	
	/**
	 * 
	 * Handle log out of the user
	 * Clear the session variable containing the user information
	 */
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout(HttpSession session, Model model) {
		session.removeAttribute("user");
		
		model.addAttribute("userDto", new UserDTO());
		model.addAttribute("credential", new UserCredential());
		return "redirect:/#login";
	}
	
	
	
	/***********************************************************
	 *                 Validation binding                      *
	 ***********************************************************/
	
	/**
	 *	Binds the Credential validator to the program 
	 */
	@InitBinder("credential")
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(credentialValidator);
	}
	
	
	/***********************************************************************
	 *                     Exception Handlers                              *
	 ***********************************************************************/
	
	
	/**
	 * Handle the exceptions on this controller
	 */
	@ExceptionHandler(ServerSideException.class)
	public ModelAndView serverSideExceptionHandler(HttpServletRequest request, Exception exception) {
		
		logger.error("Request " + request.getRequestURI() + " raised an exception " + exception);
		
		ModelAndView mv = new ModelAndView("exceptions/server_exception");
		mv.addObject("message", exception.getMessage());
		return mv;
	}	
}
