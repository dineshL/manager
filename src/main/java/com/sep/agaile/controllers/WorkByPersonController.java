package com.sep.agaile.controllers;

import org.sep.agile.model.dto.UserStoryDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sep.agaile.data.dao.UserStoryDAO;
import com.sep.agile.lists.DoneList;
import com.sep.agile.lists.InProgressList;
import com.sep.agile.lists.InTestingList;
import com.sep.agile.lists.PlannedList;
import com.sep.agile.lists.UserStoryList;

/**
 * Handles requests for the application WorkByPerson page.
 * version 1.0
 * @author Dhananjaya Subhash
 */
@Controller
@RequestMapping(value="/workbyperson")
public class WorkByPersonController {
	
    /**
     * URL: /rest/controller/viewWorkByPerson
     * viewPerson() : view work by person page
     * @param model : Model auto passed
     * @return ModelAndView
     */
	@RequestMapping(params="viewWorkByPerson" , method = RequestMethod.GET)
	public ModelAndView viewWorkByPerson(Model model) {
		System.out.println("view workbyperson controller methode was called");
		ModelAndView modelandview = new ModelAndView("user/workByPerson");
		UserStoryList userlistdto = new UserStoryDAO().getToDoList();

		PlannedList planlist = new PlannedList();
		InProgressList inprogresslist = new InProgressList();
		InTestingList intestinglist = new InTestingList();
		DoneList donelist = new DoneList();		
		
		for (UserStoryDTO u : userlistdto.getList() ) {			
			System.out.print(u.getStatus());
			String status = u.getStatus();			

			if(status.equals("Planned")) {
			   	planlist.getList().add(u);
			}
			else if(status.equals("InProgress")) {
				inprogresslist.getList().add(u);
			}
			else if(status.equals("InTesting")) {
				intestinglist.getList().add(u);
			}
			else if(status.equals("Done")) {
				donelist.getList().add(u);
			}
		    else
				System.out.println("error");			
		}
		
		modelandview.addObject("plan",planlist);
		modelandview.addObject("inprogress",inprogresslist);
		modelandview.addObject("intesting",intestinglist);
		modelandview.addObject("done",donelist);
		
		return modelandview;
	}
}

