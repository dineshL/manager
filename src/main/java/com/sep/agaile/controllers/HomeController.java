package com.sep.agaile.controllers;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.sep.agile.model.dto.UserCredential;
import org.sep.agile.model.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Controller
public class HomeController {
	/** handle requests for the home view */
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Load gome page
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		UserCredential cr = new UserCredential();
		cr.setPassword("123456789");			// test values
		cr.setUsername("dinesh");				// test values
		
		model.addAttribute("credential", cr);
		model.addAttribute("userDto" , new UserDTO());
		return "home";
	}
	
}
