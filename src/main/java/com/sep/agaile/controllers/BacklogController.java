package com.sep.agaile.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * version 1.0
 * @author Dinesh Liyanage
 *
 */

@Controller
@RequestMapping(value = "/backlog")
public class BacklogController {
	/** Controlling the backlog **/

	public BacklogController() {}
	
	/**
	 * Load the backlog view to the main frame
	 * @return String indicating the location of the file
	 * 
	 */
	@RequestMapping(params = "view", method = RequestMethod.GET)
	public String loadBacklog() {
		
		return "user/backlog";
	}
	
}
