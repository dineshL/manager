package com.sep.agaile.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.sep.agaile.data.dao.UserStoryDAO;
import com.sep.agile.lists.UserStoryList;

/**
 * Handles requests for the application person page.
 * version 1.0
 * @author Dhananjaya Subhash
  */
@Controller
@RequestMapping(value="/person")
public class PersonController {

    /**
     * URL: /rest/controller/viewPerson
     * viewPerson() : view person page
     * @param model : Model auto passed
     * @return ModelAndView
     */
	@RequestMapping(params="viewPerson" , method = RequestMethod.GET)
	public ModelAndView viewPerson(Model model) {
		System.out.println("view person controller methode was called");
		ModelAndView modelandview = new ModelAndView("user/person");
		UserStoryList userlistdto = new UserStoryDAO().getToDoList();
		modelandview.addObject("dto",userlistdto);
		return modelandview;
	}
}
