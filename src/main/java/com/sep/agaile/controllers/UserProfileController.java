package com.sep.agaile.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.sep.agile.model.dto.UserDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sep.agaile.data.dao.UserDAO;


/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Controller
@RequestMapping(value = "/profile") 
public class UserProfileController {

	@RequestMapping(params = "view", method = RequestMethod.GET)
	public ModelAndView getProfileView(HttpSession session) {

		System.out.println("user profile controller called");
		ModelAndView mv = new ModelAndView("user/profile");

		UserDTO user = new UserDAO().getUser(((UserDTO) session.getAttribute("user")).getUsername());
		if(user.getRole().trim().length() <= 0) {
			user.setRole("Developer");
		}

		List<String> userRoleList = new ArrayList<String>();
		userRoleList.add("Project Owner");
		userRoleList.add("Project Manager");
		userRoleList.add("Scrum Master");
		userRoleList.add("Developer");
		userRoleList.add("Client");

		mv.addObject("userDto", user);
		mv.addObject("userRoles", userRoleList);

		return mv;
	}

	@RequestMapping(params = "update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	public @ResponseBody boolean updateUserProfile(@RequestBody String dto, HttpSession session) {
		ObjectMapper mapper = new ObjectMapper();

		UserDTO dto2 = null;
		try {
			dto2 = mapper.readValue(dto, UserDTO.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		dto2.setId(((UserDTO) session.getAttribute("user")).getId());

		UserDAO dao = new UserDAO();
		return dao.updateUser(dto2);

	}


}
