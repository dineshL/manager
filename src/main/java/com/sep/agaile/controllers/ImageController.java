package com.sep.agaile.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Handles requests for the application upload image page.
 * version 1.0
 * @author Dhananjaya Subhash
  */
@Controller
@RequestMapping(value="/imageUpload")
public class ImageController {

	/**
	 * URL: /rest/controller/fileView
     * viewFile(): view upload file page 
     * @return String
     */
    @RequestMapping(params="fileview")
    public String viewFile() {
    	System.out.println("upload Image called ");
    	return "user/uploadFile";
    }
}
