package com.sep.agaile.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.sep.agile.model.dto.ProjectDTO;
import org.sep.agile.model.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sep.agaile.data.dao.ProjectDAO;
import com.sep.agaile.exceptions.ServerSideException;
import com.sep.agaile.exceptions.UnauthorizedAction;

@Controller
@RequestMapping(value = "/project")
public class ProjectController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@RequestMapping(params = "create", method = RequestMethod.POST)
	public String addNewProject(@ModelAttribute("project") ProjectDTO project, HttpSession session) {

		UserDTO currentUser = ((UserDTO) session.getAttribute("user"));

		if(currentUser.getRole().equals("Project Owner")) {
			ProjectDAO dao = new ProjectDAO();

			project.setProductOwner(currentUser.getId());
			dao.addNewProject(project);

			return "user/home";
		}else {
			throw new UnauthorizedAction("You are not allowed to preform this task");
		}
	}
	
	/***********************************************************************
	 *                     Exception Handlers                              *
	 ***********************************************************************/
	
	@ExceptionHandler(UnauthorizedAction.class)
	public ModelAndView serverSideExceptionHandler(HttpServletRequest request, Exception exception) {
		
		logger.error("Request " + request.getRequestURI() + " raised an exception " + exception);
		
		ModelAndView mv = new ModelAndView("exceptions/server_exception");
		mv.addObject("message", exception.getMessage());
		return mv;
	}

}
