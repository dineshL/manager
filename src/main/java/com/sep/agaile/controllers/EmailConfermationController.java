package com.sep.agaile.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Controller
@RequestMapping(value = "email")
public class EmailConfermationController {

	/** Used for the email verification process controlling
	 * 	Routes the message to the appropriate controller to take a specific action
	 */
	
	/**
	 * 
	 * @param code -> String containing the verification code
	 * @param type -> String indicating the verifying type
	 * @return String that routes the request;
	 */
	@RequestMapping(params = "verify", method = RequestMethod.GET)
	public String verifyEmailCode(@RequestParam("code") String code,
			@RequestParam("type") String type, 
			RedirectAttributes redirectAttributes) {

		redirectAttributes.addFlashAttribute("code", code);

		if(type.equals("client")) { 					//if the verifying person is a client
			return "redirect:/client?verify&code=" + code;
		}else {											//	if the verifying person is a normal user
			return "redirect:/users?verify";
		}

	}

}
