package com.sep.agaile.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.sep.agile.model.dto.UserCredential;
import org.sep.agile.model.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sep.agaile.data.dao.UserDAO;
import com.sep.agaile.exceptions.ServerSideException;
import com.sep.agaile.validators.UserDtoValidator;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@Controller
@RequestMapping(value = "/users")
public class UserController {
	
	@Autowired
	private UserDtoValidator userDtoValidator;
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	
	@RequestMapping(params = "profile")
	public String createProfile(Model model, HttpSession session) {
		logger.info("create form methord called");
		
		UserDTO dto = new UserDAO().getUser(((UserDTO) session.getAttribute("user")).getUsername());
		
		model.addAttribute("userDto", dto);
		return "user/profile";
	}
	
	
	/**
	 * 
	 * @param userDto object which is in bound with the user registration form
	 * @return String that will redirect to the appropriate jsp
	 */
	@RequestMapping(params = "register", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute("userDto") @Valid UserDTO userDto, BindingResult result,Model model, RedirectAttributes redirectAttributes) {
		logger.info("save user methord called");
		
		if(result.hasErrors()) {
			System.out.println("errors found");
			model.addAttribute("credential", new UserCredential());
			return "home";
		}
		
		UserDAO dao = new UserDAO();
		dao.insertUser(userDto);
		
		return "redirect:/email?verify&code=&type=user";
	}
	
	@RequestMapping(value = "/{username}")
	public String getUser(@PathVariable("username") String username, Map<String, Object> model) {
		
		logger.info("get user methord called");
		
		//model.put("customer", customer);
		
		return "user/view";
	}
	
	@RequestMapping(value = "/{username}/edit", method = RequestMethod.GET)
	public String editCustomerProfile(@PathVariable("username") String username, Model model) {
		
		
		return "user/edit";
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateUser(@ModelAttribute("customer") @Valid UserDTO userDto, BindingResult result) {
		logger.info("update user methord called");
		System.out.println("edit user called");
		if(result.hasErrors()) {
			return "user/register";
		}
		
		return "redirect:/users/" + userDto.getUsername();
	}
	
	@RequestMapping(value = "email", method = RequestMethod.POST, params = "ver")
	public String userVerify(@RequestParam("code") String code) {
		UserDAO dao = new UserDAO();
		Long activateUserId = dao.activateUser(code);
		if(activateUserId != -1) {
			return "redirect:/#login";
		}else {
			return "emailConfirm";
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, params = "verify")
	public String loadEmailVerficationPage() {
		return "user/email_confirm";
	}
	
	
	
	/***********************************************************************
	 *                     validator bindings                              *
	 ***********************************************************************/
	
	@InitBinder("userDto")
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(userDtoValidator);
	}
	
	/***********************************************************************
	 *                     Exception Handlers                              *
	 ***********************************************************************/
	
	@ExceptionHandler(ServerSideException.class)
	public ModelAndView serverSideExceptionHandler(HttpServletRequest request, Exception exception) {
		
		logger.error("Request " + request.getRequestURI() + " raised an exception " + exception);
		
		ModelAndView mv = new ModelAndView("exceptions/server_exception");
		mv.addObject("message", exception.getMessage());
		return mv;
	}
	
}
