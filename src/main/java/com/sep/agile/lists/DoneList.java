package com.sep.agile.lists;

import java.util.ArrayList;
import java.util.List;
import org.sep.agile.model.dto.UserStoryDTO;

/**
 * Handles requests for the application WorkByPerson page.
 * version 1.0
 * @author Dhananjaya Subhash
 */
public class DoneList {

	private List<UserStoryDTO> list = new ArrayList<UserStoryDTO>();
	
	public DoneList() {
		// TODO Auto-generated constructor stub
	}

	public List<UserStoryDTO> getList() {
		return list;
	}

	public void setList(List<UserStoryDTO> list) {
		this.list = list;
	
	
	}
}
