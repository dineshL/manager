package org.sep.agile.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.sep.agile.interfaces.Addressable;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class UserDTO implements Addressable{
	
	private long id;
	private String username;
	private String name;
	private String password;
	private String email;
	private String role;
	
	
	public UserDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public long getId() {
		return id;
	}

	@NotEmpty
	public String getUsername() {
		return username;
	}

	@NotEmpty
	public String getName() {
		return name;
	}

	@NotBlank
	public String getPassword() {
		return password;
	}

	@NotBlank
	public String getEmail() {
		return email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getLink() {
		return "http://localhost:8080/server/rest/users?name=" + this.name;
	}

}
