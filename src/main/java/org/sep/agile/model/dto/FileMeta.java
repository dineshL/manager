package org.sep.agile.model.dto;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Handles requests for the application file upload page.
 * version 1.0
 * @author Dhananjaya Subhash
 */

//ignore "bytes" when return json format
@JsonIgnoreProperties({"bytes"}) 
public class FileMeta {    

	private String fileName;
	private String fileSize;
    private String fileType;
    private byte[] bytes;
    
    public FileMeta() {
		super();
	}
    
    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}


    
}
