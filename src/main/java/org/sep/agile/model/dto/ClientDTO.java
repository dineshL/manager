package org.sep.agile.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class ClientDTO extends UserDTO{
	
	private String organization;
	private String contactNumber;
	private String project;
	
	public ClientDTO() {
		super();
	}

	public String getOrganization() {
		return organization;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public String getProject() {
		return project;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public void setProject(String project) {
		this.project = project;
	}

	
}
