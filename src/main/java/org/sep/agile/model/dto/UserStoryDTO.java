package org.sep.agile.model.dto;

import java.util.Date;

import org.sep.agile.interfaces.Addressable;

public class UserStoryDTO implements Addressable{
	
	String name;
	long id;
	Date startingdate;
	Date enddate;
	String duration;
	String projectid;
	String sprintid;
	String status;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getStartingdate() {
		return startingdate;
	}
	public void setStartingdate(Date startingdate) {
		this.startingdate = startingdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
	public String getSprintid() {
		return sprintid;
	}
	public void setSprintid(String sprintid) {
		this.sprintid = sprintid;
	}
	
	@Override
	public String getLink() {
		return "http://localhost:8080/server/rest/mywork?name=" + this.name;
	}
}
