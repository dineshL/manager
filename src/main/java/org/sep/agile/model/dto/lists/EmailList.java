package org.sep.agile.model.dto.lists;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class EmailList {
	
	private List<String> list = new ArrayList<String>();
	
	public EmailList() {
		// TODO Auto-generated constructor stub
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

}
