package org.sep.agile.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class UserCredential {

	private String username;
	private String password;
	
	public UserCredential() {
		// TODO Auto-generated constructor stub
	}

	public UserCredential(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	@NotBlank
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@NotBlank
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}
