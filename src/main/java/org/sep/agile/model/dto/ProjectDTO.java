package org.sep.agile.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class ProjectDTO {

	private long id;
	private String name;
	private long productOwner;
	private long scrumMaster;
	private String client;
	

	public ProjectDTO() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public long getProductOwner() {
		return productOwner;
	}

	public long getScrumMaster() {
		return scrumMaster;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setProductOwner(long productOwner) {
		this.productOwner = productOwner;
	}

	public void setScrumMaster(long scrumMaster) {
		this.scrumMaster = scrumMaster;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}


}
