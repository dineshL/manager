package org.sep.agile.model.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Version 1.0
 * @author Dinesh Liyanage
 *
 */
@XmlRootElement
public class Message {

	private static Message instance = null;
	
	private String message;
	private int code;
	
	private Message() {
		// TODO Auto-generated constructor stub
	}

	public static Message compose() {
		if(instance == null) {
			instance = new Message();
			System.out.println("new message created");
		}
		System.out.println("Message " + instance.getMessage());
		return instance;
	}
	
	public Message(int code, String message) {
		this.message = message;
		this.code = code;
	}



	public String getMessage() {
		return message;
	}

	public int getCode() {
		return code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setCode(int code) {
		this.code = code;
	}
	
	
}
