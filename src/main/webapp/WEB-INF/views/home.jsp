
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Agile Project Management Software | Scrum & Kanban Agile
	Development Tool | Lean Project Management Training</title>
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="resources/css/bootstrap.min.css"
	type="text/css">

<!-- Custom Fonts -->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="resources/font-awesome/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" type="text/css"
	href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.5.4/bootstrap-select.min.css">
<!-- Plugin CSS -->
<link rel="stylesheet" href="resources/css/animate.min.css"
	type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="resources/css/creative.css" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">
	window.onload = function() {
		if (window.jQuery) {
			$("#registerBtn").prop('disabled', true);

			$("input[type=password]").keyup(function() {
				var ucase = new RegExp("[A-Z]+");
				var lcase = new RegExp("[a-z]+");
				var num = new RegExp("[0-9]+");

				if ($("#password1").val() == $("#password2").val()) {
					$("#pwmatch").removeClass("glyphicon-remove");
					$("#pwmatch").addClass("glyphicon-ok");
					$("#pwmatch").css("color", "#00A41E");
					$("#registerBtn").prop('disabled', false);
				} else {
					$("#pwmatch").removeClass("glyphicon-ok");
					$("#pwmatch").addClass("glyphicon-remove");
					$("#pwmatch").css("color", "#FF0004");
					$("#registerBtn").prop('disabled', true);
				}
			});
		}
	}
</script>
</head>

<body id="page-top">

	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand page-scroll" href="#page-top">A P M S</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="page-scroll" href="#about">About</a></li>
					<li><a class="page-scroll" href="#services">Services</a></li>
					<li><a class="page-scroll" href="#portfolio">Portfolio</a></li>
					<li><a class="page-scroll" href="#contact">Contact</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

	<header>
		<div class="header-content">
			<div class="header-content-inner">
				<h1>Visual Project</h1>
				<h1>Management Software</h1>
				<hr>
				<p>Agile Project Management Software | Scrum & Agile Development
					Tool | Lean Project Management Training</p>
				<a href="#login" class="btn btn-primary btn-xl page-scroll">Log
					In</a> <a href="#register" class="btn btn-primary btn-xl page-scroll">Register</a>
			</div>
		</div>
	</header>

	<section class="bg-primary" id="login">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 text-center">
					<h2 class="section-heading">Good afternoon!</h2>
					<hr class="light">
					<p class="text-faded">It's going to be a great day</p>
					<div class="row">
						<form:form action="login" method="POST"
							modelAttribute="credential">
							<div class="col-lg-4 col-lg-offset-4">
								<div class="form-group">
									<div class="">
										<form:input id="textinput" placeholder="Username"
											class="form-control input-md" path="username" />
										<form:errors path="username" class="label label-danger"></form:errors>
									</div>
								</div>

								<!-- Password input-->
								<div class="form-group">
									<div class="">
										<form:input id="passwordinput" placeholder="Password"
											class="form-control input-md" type="password" path="password" />
										<form:errors path="password" class="label label-danger"></form:errors>
									</div>
								</div>
								<button type="submit" class="btn btn-default btn-xl">Log
									In</button>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="register">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">At Your Service</h2>
					<hr class="primary">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="container-fluid">
					<section class="container">
						<form:form action="users?register" method="post"
							modelAttribute="userDto">
							<div class="container-page">
								<div class="col-md-6">
									<h3 class="dark-grey">Registration</h3>

									<div class="form-group col-lg-12">
										<label>Full Name</label>
										<form:input placeholder="Username" path="name"
											class="form-control input-md" type="text"
											pattern="[A-Za-z ]{1,20}" title="valid name is required"
											required="required" />
										<form:errors path="name" class="label label-danger"></form:errors>
									</div>

									<div class="form-group col-lg-6">
										<label>Username</label>
										<form:input id="textinput" name="textinput"
											placeholder="Username" class="form-control input-md"
											type="text" path="username"
											pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$"
											title="valid username is required" required="required" />
										<form:errors path="username" class="label label-danger"></form:errors>
									</div>

									<div class="form-group col-lg-6">
										<label>Email Address</label>
										<form:input id="textinput" name="textinput"
											placeholder="Username" path="email"
											class="form-control input-md" type="email"
											title="valid email is required" required="required" />
										<form:errors path="email" class="label label-danger"></form:errors>
									</div>

									<div class="form-group col-lg-6">
										<label>Password</label>
										<form:input id="password1" name="textinput"
											placeholder="password" class="form-control input-md"
											type="password" path="password" />
										<form:errors path="password" class="label label-danger"></form:errors>
									</div>

									<div class="form-group col-lg-6">
										<label>Repeat Password</label> <input id="password2"
											name="textinput" placeholder="Confirm password"
											class="form-control input-md" type="password" />
									</div>
									<span id="pwmatch" class="glyphicon glyphicon-remove"
										style="color: #FF0004; display: hidden"> Passwords
										Match</span>
								</div>
								<div class="col-md-6">
									<h3 class="dark-grey">Terms and Conditions</h3>
									<p>By clicking on "Register" you agree to The Company's'
										Terms and Conditions</p>
									<p>While rare, prices are subject to change based on
										exchange rate fluctuations - should such a fluctuation happen,
										we may request an additional payment. You have the option to
										request a full refund or to pay the new price. (Paragraph
										13.5.8)</p>
									<p>Should there be an error in the description or pricing
										of a product, we will provide you with a full refund
										(Paragraph 13.5.6)</p>
									<p>Acceptance of an order by us is dependent on our
										suppliers ability to provide the product. (Paragraph 13.5.6)</p>

									<input type="submit" class="btn btn-primary" value="Register"
										id="registerBtn" />
								</div>
							</div>
						</form:form>
					</section>
				</div>
			</div>
		</div>
	</section>

	<!-- jQuery -->
	<script src="resources/js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="resources/js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fittext.js"></script>
	<script src="resources/js/wow.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="resources/js/creative.js"></script>

</body>

</html>
