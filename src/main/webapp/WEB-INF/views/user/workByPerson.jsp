<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Work By Person</title>
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="resources/css/workbyperson.css" type="text/css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
	  <h2>Work By Person</h2>
	             
	  <table class="table table-bordered" >
	    <thead>
	      <tr>
	        <th>Person</th>
	        <th>Planned</th>
	        <th>In progress</th>
	        <th>In Testing</th>
	        <th>Done</th>
	      </tr>
	    </thead>
	    <tbody>   
		    <tr>
		    	<td id="userstory">janith kularathne</td>
		    	
		    	<c:forEach var="planned" items="${plan.list}">
		        <td id="userstory">${planned.name}</td>  
		        </c:forEach>  
		        
		        <c:forEach var="inprogressed" items="${inprogress.list}">
		        <td id="userstory">${inprogressed.name}</td>  
		        </c:forEach>  
		        
		        <c:forEach var="intested" items="${intesting.list}">
		        <td id="userstory">${intested.name}</td>  
		        </c:forEach> 
		        
		        <c:forEach var="doned" items="${done.list}">
		        <td id="userstory">${doned.name}</td>  
		        </c:forEach> 
		   </tr>
	    </tbody>
	  </table>
	</div>
</body>
</html>