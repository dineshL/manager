<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Upload File</title>
	
	<script src="resources/js/jquery.1.9.1.min.js"></script>
	<script src="resources/js/jquery.ui.widget.js"></script>
	<script src="resources/js/jquery.iframe-transport.js"></script>
	<script src="resources/js/jquery.fileupload.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/myuploadfunction.js"></script>
	
	<link href="resources/css/bootstrap.css" type="text/css" rel="stylesheet">	
	<link href="resources/css/dropzone.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<h3>Upload Your File</h3>
	<div style="width:500px;padding:20px">	 
	    <input id="fileupload" type="file" name="files[]" data-url="controller/upload" multiple>
	 	<button type="button" >Upload</button>
	 	
	    <!-- <div id="dropzone">Drop files here</div> 
	 
	    <div id="progress">
	        <div style="width: 0%;"></div>
	    </div>
	 
	     <table id="uploaded-files">
	        <tr>
	            <th>File Name</th>
	            <th>File Size</th>
	            <th>File Type</th>
	            <th>Download</th>
	        </tr>
	    </table> -->
	 
	</div>
</body>
</html>