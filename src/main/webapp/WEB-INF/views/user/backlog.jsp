<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<section class="content-header">
	<h1>
		Backlog <small>some text</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Backlog</li>
	</ol>
</section>
<!-- Main content -->
<section class="content" id="mainContainer">
	<div class="col-sm-4">
		<div class="list-group" id="sortable"
			style="height: 480px; overflow: auto;">
			<div class="item bg-gray">
				<div class="external-event">
					<a href="javascript:void(0)" class="product-title">User story 1</a><span
						class="pull-right badge bg-green">2hr</span><br>
					<label style="font-size: 11px; color: white">As a user i
						want to log into the system, so that i can work on my projects</label>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-8 bg-gray" id="detaildView">
		<div class="nav nav-tabs-custom"
			style="background-color: transparent; border-color: transparent; box-shadow: none">
			<label class="pull-left header"> User
				story information User story information User story information User
				story information User story information User story information User
				story information User story information User story information
				</label>

				<ul class="nav nav-tabs pull-right" style="border-bottom: none">
					<li><a href="#tab_1-1" data-toggle="tab"
						style="background-color: transparent">Tab 1</a></li>
					<li><a href="#tab_2-2" data-toggle="tab"
						style="background-color: transparent">Tab 2</a></li>
					<li class="active"><a href="#tab_3-2" data-toggle="tab"
						style="background-color: transparent">Basic</a></li>
				</ul>
				<div class="tab-content"
					style="background-color: transparent; color: white; font-size: 12px;">
					<div class="tab-pane active" id="tab_1-1">
						<b>How to use:</b>

						<p>
							Exactly like the original bootstrap tabs except you should use
							the custom wrapper
							<code>.nav-tabs-custom</code>
							to achieve this style.
						</p>
						A wonderful serenity has taken possession of my entire soul, like
						these sweet mornings of spring which I enjoy with my whole heart.
						I am alone, and feel the charm of existence in this spot, which
						was created for the bliss of souls like mine. I am so happy, my
						dear friend, so absorbed in the exquisite sense of mere tranquil
						existence, that I neglect my talents. I should be incapable of
						drawing a single stroke at the present moment; and yet I feel that
						I never was a greater artist than now.
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_2-2">The European languages are
						members of the same family. Their separate existence is a myth.
						For science, music, sport, etc, Europe uses the same vocabulary.
						The languages only differ in their grammar, their pronunciation
						and their most common words. Everyone realizes why a new common
						language would be desirable: one could refuse to pay expensive
						translators. To achieve this, it would be necessary to have
						uniform grammar, pronunciation and more common words. If several
						languages coalesce, the grammar of the resulting language is more
						simple and regular than that of the individual languages.</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="tab_3-2">Lorem Ipsum is simply
						dummy text of the printing and typesetting industry. Lorem Ipsum
						has been the industry's standard dummy text ever since the 1500s,
						when an unknown printer took a galley of type and scrambled it to
						make a type specimen book. It has survived not only five
						centuries, but also the leap into electronic typesetting,
						remaining essentially unchanged. It was popularised in the 1960s
						with the release of Letraset sheets containing Lorem Ipsum
						passages, and more recently with desktop publishing software like
						Aldus PageMaker including versions of Lorem Ipsum.</div>
				</div>
		</div>
	</div>
</section>
<script>
$(".item").click(function() {
	var title = $(this).find('a.product-title').text();

	$("label.header").text(title);
	
	});
	
	$(function() {
		$("#sortable").sortable();
		$("#sortable").disableSelection();
	});

	$("#sortable").sortable({
		stop : function(event, ui) {
			var data = "";

			$("#sortable > div").each(function(i, el) {
				var p = $(el).attr("id");
				data += p + "=" + $(el).index() + ",";
			});

			//statusMessage(data.slice(0, -1));
		}
	});
</script>
