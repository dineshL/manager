<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<section class="content-header">
	<h1>
		Profile <small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Profile</li>
	</ol>
</section>

<!-- Main content -->
<section class="content" id="mainContainer">
	<!-- /.content -->
	<!-- Profile Image -->
	<div class="row">
		<div class="col-md-3">

			<!-- Profile Image -->
			<div class="box box-primary">
				<div class="box-body box-profile">
					<!-- <img class="profile-user-img img-responsive img-circle"
						src="../../dist/img/user4-128x128.jpg" alt="User profile picture"> -->
					<h3 class="profile-username text-center">${userDto.name}</h3>
					<p class="text-muted text-center" id="lblUserRole">${userDto.role}</p>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->

			<!-- About Me Box -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">About Me</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<strong><i class="fa fa-book margin-r-5"></i> Education</strong>
					<p class="text-muted">B.S. in Computer Science from the
						University of Tennessee at Knoxville</p>

					<hr>

					<strong><i class="fa fa-map-marker margin-r-5"></i>
						Location</strong>
					<p class="text-muted">Malibu, California</p>

					<hr>

					<strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
					<p>
						<span class="label label-danger">UI Design</span> <span
							class="label label-success">Coding</span> <span
							class="label label-info">Javascript</span> <span
							class="label label-warning">PHP</span> <span
							class="label label-primary">Node.js</span>
					</p>

					<hr>

					<strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Etiam fermentum enim neque.</p>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
		<div class="col-md-9">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li><a href="#activity" data-toggle="tab">Open Assignments</a></li>
					<li><a href="#timeline" data-toggle="tab">Timeline</a></li>
					<li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane" id="activity">
						<div class="row">
							<div class="col-xs-12">
								<div class="box">
									<div class="box-header">
										<h3 class="box-title">Open Assignments</h3>
									</div>
									<!-- /.box-header -->
									<div class="box-body table-responsive no-padding">
										<table class="table table-hover">
											<tr>
												<th>ID</th>
												<th>User story</th>
												<th>Date</th>
												<th>Status</th>
											</tr>
											<!-- <tr>
												<td>183</td>
												<td>John Doe</td>
												<td>11-7-2014</td>
												<td><span class="label label-success">Approved</span></td>
												<td>Bacon ipsum dolor sit amet salami venison chicken
													flank fatback doner.</td>
											</tr>
											<tr>
												<td>219</td>
												<td>Alexander Pierce</td>
												<td>11-7-2014</td>
												<td><span class="label label-warning">Pending</span></td>
												<td>Bacon ipsum dolor sit amet salami venison chicken
													flank fatback doner.</td>
											</tr>
											<tr>
												<td>657</td>
												<td>Bob Doe</td>
												<td>11-7-2014</td>
												<td><span class="label label-primary">Approved</span></td>
												<td>Bacon ipsum dolor sit amet salami venison chicken
													flank fatback doner.</td>
											</tr>
											<tr>
												<td>175</td>
												<td>Mike Doe</td>
												<td>11-7-2014</td>
												<td><span class="label label-danger">Denied</span></td>
												<td>Bacon ipsum dolor sit amet salami venison chicken
													flank fatback doner.</td>
											</tr> -->
										</table>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
						</div>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="timeline"></div>
					<!-- /.tab-pane -->

					<div class="active tab-pane" id="settings">
						<form:form class="form-horizontal" id="userForm"
							action="profile?update" modelAttribute="userDto">
							<div class="form-group">
								<label for="inputName" class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									<form:input type="text" class="form-control" path="name"
										pattern="[A-Za-z ]{1,30}" title="valid name is required"
										required="required" />
									<form:errors path="name" class="label label-danger"></form:errors>
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-sm-2 control-label">Username</label>
								<div class="col-sm-10">
									<form:input class="form-control" path="username"
										pattern="[A-Za-z ]{1,30}" title="valid username is required"
										required="required" />
									<form:errors path="username" class="label label-danger"></form:errors>
								</div>
							</div>
							<div class="form-group">
								<label for="inputName" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-10">
									<form:input class="form-control" path="email"
										title="valid email is required" required="required" />
									<form:errors path="email" class="label label-danger"></form:errors>
								</div>
							</div>
							<div class="form-group">
								<label for="inputExperience" class="col-sm-2 control-label">Priviledge</label>
								<div class="col-sm-10">
									<form:select path="role" items="${userRoles}"
										class="form-control" multiple="false" id="userRoleSelect" />
								</div>
							</div>
							<!-- <div class="form-group">
								<label for="inputSkills" class="col-sm-2 control-label">Skills</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="inputSkills"
										placeholder="Skills">
								</div>
							</div> -->
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-info btn-flat pull-right"
										style="width: 100px;">Save</button>
								</div>
							</div>
							<div id="alert"></div>
						</form:form>
					</div>
					<!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			</div>
			<!-- /.nav-tabs-custom -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<script type='text/javascript'>
	$.fn.serializeObject = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	$('#userForm')
			.submit(
					function(event) {

						var json = JSON.stringify($('#userForm')
								.serializeObject());

						$
								.ajax({
									url : $("#userForm").attr("action"),
									data : json,
									type : "POST",

									beforeSend : function(xhr) {
										xhr.setRequestHeader("Accept",
												"application/json");
										xhr.setRequestHeader("Content-Type",
												"application/json");
									},
									success : function(user) {

										var strVar = "";
										strVar += "<div class=\"alert alert-success alert-dismissible\" role=\"alert\" id=\"alert\" >";
										strVar += "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;<\/span><\/button>";
										strVar += "  <strong>Success!<\/strong><div id=\"result\"><\/div>";
										strVar += "<\/div>";

										$("#result").html(user.role);
										$("#alert").append(strVar);
									}
								});

						event.preventDefault();
					});
</script>