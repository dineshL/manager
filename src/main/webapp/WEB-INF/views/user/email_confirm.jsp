<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Agile Project Management Software | Scrum Development
	Tool | Lean Project Management Training</title>

<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="resources/css/bootstrap.min.css"
	type="text/css">

<!-- Custom Fonts -->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="resources/font-awesome/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" type="text/css"
	href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.5.4/bootstrap-select.min.css">
<!-- Plugin CSS -->
<link rel="stylesheet" href="resources/css/animate.min.css"
	type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="resources/css/creative.css" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
</head>
<body>
	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top affix"
		style="background-color: white">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand page-scroll" href="#page-top"
					style="color: #f05f40;">A P M S</a>
			</div>
		</div>
		<!-- /.container-fluid -->
	</nav>

	<div>
		<div class="header-content"></div>
	</div>

	<section id="register">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">At Your Service</h2>
					<hr class="primary">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">

				<div class="col-md-6">
					<!-- Horizontal Form -->

					<div class="box box-info">
						<!-- /.box-header -->
						<!-- form start -->
						<form class="form-horizontal" action="users/email?ver" method="post">
							<div class="box-body">
								<div class="form-group">
									<h3 class="dark-grey">Terms and Conditions</h3>
					<p>Please enter the code came along with the email to activate your account</p>
									<div class="col-sm-10">
										<input type="text" class="form-control"
											placeholder="Please enter the code came along with the email"
											name = "code"
											value="${code}" >
									</div>
								</div>
								<div class="form-group">

									<div class="col-sm-10">
										<button type="submit" class="btn btn-primary pull-right">Activate your account</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<!-- /.box -->
				</div>
				<div class="col-md-5 col-md-offset-1">
					<h3 class="dark-grey">Terms and Conditions</h3>
					<p>By clicking on "Register" you agree to The Company's' Terms
						and Conditions</p>
					<p>While rare, prices are subject to change based on exchange
						rate fluctuations - should such a fluctuation happen, we may
						request an additional payment. You have the option to request a
						full refund or to pay the new price. (Paragraph 13.5.8)</p>
					<p>Should there be an error in the description or pricing of a
						product, we will provide you with a full refund (Paragraph 13.5.6)</p>
					<p>Acceptance of an order by us is dependent on our suppliers
						ability to provide the product. (Paragraph 13.5.6)</p>
				</div>
			</div>
		</div>
	</section>


	<!-- jQuery -->
	<script src="resources/js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="resources/js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fittext.js"></script>
	<script src="resources/js/wow.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="resources/js/creative.js"></script>

</body>
</html>