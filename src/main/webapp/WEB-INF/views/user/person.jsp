<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Bootstrap Case</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/person.css" type="text/css">
  
    <!-- <script src="resources/js/person.js"></script>  -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="resources/js/verticalTabs.js"></script>
</head>
<body>  
  <h2>People</h2>
  <div id="content">
	   <div id="tab-container">
	      <ul>
	         <li id="janith1"><a href="#janith" data-toggle="tab" onclick="janith()">Janith</a></li>
	         <li id="vishwa1"><a href="#vishwa" data-toggle="tab" onclick="vishwa()">Vishwa</a></li>
	         <li id="subhash1"><a href="#subhash" data-toggle="tab" onclick="subhash()">Subhash</a></li>
	         <li id="dinesh1"><a href="#dinesh" data-toggle="tab" onclick="dinesh()">Dinesh</a></li>
	      </ul>
	   </div>
	   <div class="tab-content">
		   <div id="janith" class="tab-pane fade in active">
			   <div id="main-container">
				   <div class="container">  
					   <h4><b>Janith Kularathne</b></h4>
					   <ul class="nav nav-tabs" style="border-bottom:0px">
						    <li class="active"><a data-toggle="tab" href="#menu1">Open Assignment</a></li>
						    <li><a data-toggle="tab" href="#menu2">Projects and Teams</a></li>
						    <li><a data-toggle="tab" href="#menu3">Allocations</a></li>
					   </ul>				  
					   <div class="tab-content">
						    <div id="menu1" class="tab-pane fade in active" style="margin-top:-250px">						    
								<table class="table table-bordered" style="width:77%" >
							        <tbody>
							   			<c:forEach var="stories1" items="${dto.list}">
							    		<tr>						    	
							        		<td id="userstory">${stories1.name}</td>						        
							      		</tr>
										</c:forEach>					      
							    	</tbody>
							    </table>
						   </div>					
						   <div id="menu2" class="tab-pane fade">
						     <h4>Projects and Teams here</h4>
						   </div>
						   <div id="menu3" class="tab-pane fade">
						     <h4>Allocations here</h4>
						   </div>
					  </div>
				  </div>
			   </div>
		   </div> 
		   <div id="vishwa" class="tab-pane fade">
			   <div id="main-container">
				   <div class="container">  
					   <h4><b>Vishwa Rathnayake</b></h4>
					   <ul class="nav nav-tabs" style="border-bottom:0px">
						    <li class="active"><a data-toggle="tab" href="#menu1">Open Assignment</a></li>
						    <li><a data-toggle="tab" href="#menu2">Projects and Teams</a></li>
						    <li><a data-toggle="tab" href="#menu3">Allocations</a></li>
					   </ul>				  
					   <div class="tab-content">
						    <div id="menu1" class="tab-pane fade in active" style="margin-top:-250px">						    
								<table class="table table-bordered" style="width:77%" >
							        <tbody>
							   			<c:forEach var="stories1" items="${dto.list}">
							    		<tr>						    	
							        		<td id="userstory">${stories1.name}</td>						        
							      		</tr>
										</c:forEach>					      
							    	</tbody>
							    </table>
						   </div>					
						   <div id="menu2" class="tab-pane fade">
						     <h4>Projects and Teams here</h4>
						   </div>
						   <div id="menu3" class="tab-pane fade">
						     <h4>Allocations here</h4>
						   </div>
					  </div>
				  </div>
			   </div>
		   </div> 
		   <div id="subhash" class="tab-pane fade">
			   <div id="main-container">
				   <div class="container">  
					   <h4><b>Subhash Nawarathne</b></h4>
					   <ul class="nav nav-tabs" style="border-bottom:0px">
						    <li class="active"><a data-toggle="tab" href="#menu1">Open Assignment</a></li>
						    <li><a data-toggle="tab" href="#menu2">Projects and Teams</a></li>
						    <li><a data-toggle="tab" href="#menu3">Allocations</a></li>
					   </ul>				  
					   <div class="tab-content">
						    <div id="menu1" class="tab-pane fade in active" style="margin-top:-250px">						    
								<table class="table table-bordered" style="width:77%" >
							        <tbody>
							   			<c:forEach var="stories1" items="${dto.list}">
							    		<tr>						    	
							        		<td id="userstory">${stories1.name}</td>						        
							      		</tr>
										</c:forEach>					      
							    	</tbody>
							    </table>
						   </div>					
						   <div id="menu2" class="tab-pane fade">
						     <h4>Projects and Teams here</h4>
						   </div>
						   <div id="menu3" class="tab-pane fade">
						     <h4>Allocations here</h4>
						   </div>
					  </div>
				  </div>
			   </div>
		   </div> 
		   <div id="dinesh" class="tab-pane fade">
			   <div id="main-container">
				   <div class="container">  
					   <h4><b>Dinesh Karunarathne</b></h4>
					   <ul class="nav nav-tabs" style="border-bottom:0px">
						    <li class="active"><a data-toggle="tab" href="#menu1">Open Assignment</a></li>
						    <li><a data-toggle="tab" href="#menu2">Projects and Teams</a></li>
						    <li><a data-toggle="tab" href="#menu3">Allocations</a></li>
					   </ul>				  
					   <div class="tab-content">
						    <div id="menu1" class="tab-pane fade in active" style="margin-top:-250px">						    
								<table class="table table-bordered" style="width:77%" >
							        <tbody>
							   			<c:forEach var="stories1" items="${dto.list}">
							    		<tr>						    	
							        		<td id="userstory">${stories1.name}</td>						        
							      		</tr>
										</c:forEach>					      
							    	</tbody>
							    </table>
						   </div>					
						   <div id="menu2" class="tab-pane fade">
						     <h4>Projects and Teams here</h4>
						   </div>
						   <div id="menu3" class="tab-pane fade">
						     <h4>Allocations here</h4>
						   </div>
					  </div>
				  </div>
			   </div>
		   </div> 
	   </div> 
   </div>
</body>
</html>