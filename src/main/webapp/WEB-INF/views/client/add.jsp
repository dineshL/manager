<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<section class="content-header">
	<h1>
		Client <small>add new client</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">client</li>
	</ol>
</section>
<!-- Main content -->
<section class="content" id="mainContainer">
	<div class="col-md-7">
		<!-- Horizontal Form -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Add a new clinet</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form:form class="form-horizontal" action="client?add" method="post"
				modelAttribute="clientDto">
				<div class="box-body">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
							<form:input type="text" class="form-control"
								placeholder="name of the client" path="name"
								pattern="[A-Za-z ]{1,50}" title="valid name is required"
								required="required" />
							<form:errors path="name" class="label label-danger"></form:errors>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<form:input type="email" class="form-control"
								placeholder="e-mail" path="email"
								title="valid email is required" required="required" />
							<form:errors path="email" class="label label-danger"></form:errors>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Organization</label>
						<div class="col-sm-10">
							<form:input type="text" class="form-control"
								placeholder="organization" path="organization"
								pattern="[A-Za-z ]{1,50}"
								title="valid organozation name is required" required="required" />
							<form:errors path="organization" class="label label-danger"></form:errors>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword3" class="col-sm-2 control-label">Contact
							number</label>
						<div class="col-sm-10">
							<form:input type="text" class="form-control"
								placeholder="contact number" path="contactNumber" 
								pattern="[0][1-9][1-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]"
								title="valid contact number is required" 
								required="required"/>
							<form:errors path="organization" class="label label-danger"></form:errors>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-info btn-flat pull-right"
						style="width: 100px;">Save</button>
				</div>
			</form:form>
		</div>
		<!-- /.box-body -->
		<!-- /.box-footer -->
	</div>
	<!-- /.box -->
	<div class="col-md-4">
		<div class="pad margin no-print" style="margin-top: -10px;">
			<div class="callout callout-info"
				style="margin-bottom: 0 !important;">
				<h4>
					<i class="fa fa-info"></i> Note:
				</h4>
				Client will receive an email containing a link to sign into project manager upon this registration.
			</div>
		</div>
	</div>
</section>