<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="modal fade" id="createProjectModel" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true"></button>
				<h4 class="modal-title" id="myModalLabel">New Project</h4>
			</div>
			<div class="modal-body">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Add a new clinet</h3>
					</div>
					<form:form method="POST" id="projectForm" action="project?create"
						modelAttribute="project" class="form-horizontal">
						<div class="box-body">
							<div class="form-group">
								<label for="projectName" class="col-sm-2 control-label">Project
									name</label>
								<div class="col-sm-10">
									<form:input placeholder="Project name"
										class="form-control input-md" name="name" path="name"
										pattern="[A-Za-z ]{1,20}" type="text"
										title="valid project name is required" required="required" />
								</div>
							</div>
							<div class="form-group">
								<label for="clientEmail" class="col-sm-2 control-label">Client</label>
								<div class="col-sm-10">
									<form:select path="client" items="${clientEmails.list}"
										class="form-control" multiple="false" id="userRoleSelect" />
								</div>
							</div>
							<div class="box-footer">
								<input class="btn btn-info btn-flat pull-right" type="submit"
									value="Save" id="save" style="width: 100px;" />
							</div>
						</div>
					</form:form>
				</div>
			</div>
			<div class="modal-footer">
				<!-- <div id="alert"></div> -->
				<button type="button" class="btn btn-flat btn-info"
					data-dismiss="modal">Close</button>

				<button type="button" class="btn btn-info btn-flat"
					style="float: left" onclick="loadClientAdd();" data-dismiss="modal">Add
					Client</button>
			</div>
		</div>
	</div>
</div>
<script type='text/javascript'>
	/* window.onload = function() {
		if (window.jQuery) {

			$.fn.serializeObject = function() {
				var o = {};
				var a = this.serializeArray();
				$.each(a, function() {
					if (o[this.name] !== undefined) {
						if (!o[this.name].push) {
							o[this.name] = [ o[this.name] ];
						}
						o[this.name].push(this.value || '');
					} else {
						o[this.name] = this.value || '';
					}
				});
				return o;
			};

			$("#save").click(function() {

				$("#projectForm").submit();
			});

			$("#projectForm")
					.submit(
							function(event) {
								var json = JSON.stringify($('#projectForm')
										.serializeObject());
								alert($("#projectForm").attr("action"));

								$
										.ajax({
											url : $("#projectForm").attr(
													"action"),
											data : json,
											type : "POST",

											beforeSend : function(xhr) {
												xhr.setRequestHeader("Accept",
														"application/json");
												xhr.setRequestHeader(
														"Content-Type",
														"application/json");
											},
											success : function(message) {
												var strVar = "";
												strVar += "<div class=\"alert alert-success alert-dismissible\" role=\"alert\" id=\"alert\" >";
												strVar += "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;<\/span><\/button>";
												strVar += "  <strong>Success!<\/strong><div id=\"result\"><\/div>";
												strVar += "<\/div>";

												//$("#result").html(message);
												alert(message);
												$("#alert").append(strVar);
											},
											error : function(e) {
												console.log("ERRORDD: ", e);
												console.log("this is an error");
											},
											done : function(e) {
												console.log("DONE");
											}
										});

								event.preventDefault();
							});

		} else {
			// jQuery is not loaded
			alert("jQuery is not loaded");
		}
	} */
</script>
<!-- end of model -->
