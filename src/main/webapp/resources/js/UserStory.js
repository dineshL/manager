/**
 * 
 */

// for implement drag and drop 
function allowdrop(ev) {
	ev.preventDefault();
}
function drag(ev) {
	ev.dataTransfer.setData("content",ev.target.id);
}
/*for mywork drop event*/
function dropMyWork(ev) {
	ev.preventDefault();
	var content = ev.dataTransfer.getData("content");
	ev.target.appendChild(document.getElementById(content));
	checknumber();
	update(ev.target.id,content);

} 
/*for estimation drop event*/
function dropEstimation(ev) {
	ev.preventDefault();
	var content = ev.dataTransfer.getData("content");
	ev.target.appendChild(document.getElementById(content));
	countEstimateUserStories();
	update(ev.target.id,content);

} 


// for rightclick dropdown menue
$(".userstory").bind("contextmenu", function (event) {
    
    event.preventDefault();
    localStorage['userstoryid'] = $(this).attr('id');
    localStorage['userstory'] = $(this).text();
    // Show contextmenu
    $(".custom-menu").finish().toggle(100).
    
    // In the right position (the mouse)
    css({
        top: event.pageY + "px",
        left: event.pageX + "px"
    });
});

$(document).bind("mousedown", function (e) {
    // If the clicked element is not the menu
    if (!$(e.target).parents(".custom-menu").length > 0) {
        
        // Hide it
        $(".custom-menu").hide(100);
    }
});

$(".custom-menu li").click(function(e){
   
	/*var id = localStorage['userstoryid'];
	$("newtab").setAttribute("href","userstory?newtab="+id);*/
	
	 // This is the triggered action name
    switch($(this).attr("data-action")) {
        
        // A case for each action.
    	case "Open in new tab" : $.get("userstory?newtab="+localStorage['userstoryid'],function(data){
    							$(".container").html(data);
    							});
    							 break;
        case "Rename":
        			  alert("rename");
        			  RenameUserStory(localStorage['userstory'], localStorage['userstoryid']);
        			  break;
        case "Delete": 
					  deleteUserStory( localStorage['userstoryid']);
        			  break;
    }
  
    // Hide it AFTER the action was triggered
    $(".custom-menu ").hide(100);
  });
  
function  deleteUserStory(storyid) {
	
	$.post("userstory?delete="+storyid);
	
		
}

function callMyWork() {
	$.get("userstory?work" ,function( data ) {
		$( "#mainContainer" ).html ( data ) ;
	});
}
function newTab() {
	 
	 var id = localStorage['userstoryid'];
	 $.post("userstory?newtab="+id);
}

function RenameUserStory(story,id) {
	$("#email").html(story); 
	$("#popupformtitle").html("Change User Story");
	$("#popupform").attr("action","userstory?rename");
	$("#userstoryid").text(id);
	/*$.post("userstory?rename="+id+"&story="+story);*/
	

}
function change() {
	$("#popupformtitle").html("Add new user story");
	$("#email").html("");
	
}
function  deleteUserStory(storyid) {
	
	$.post("userstory?delete="+storyid, function() {
	alert("OK");
	})
		
}

function update(duration,id) {
	$.post("estimations?update="+id+"&duration="+duration);
}

function countEstimateUserStories() {
   var zerohr = $('#0 > div').length; 
   var onehr = $('#1 > div').length;
   var twohr =  $('#2 > div').length;
   var threehr =  $('#3 > div').length;
   var fivehr =  $('#5 > div').length;
   document.getElementById("zerohr").innerHTML = zerohr;
   document.getElementById("onehr").innerHTML = onehr;
   document.getElementById("twohr").innerHTML = twohr;
   document.getElementById("threehr").innerHTML = threehr;
   document.getElementById("fivehr").innerHTML = fivehr;  	
}

function checknumber() {
    var tododivCount = $('#todo > div').length;
	var planeddivCount = $('#planned > div').length;
	var testingdivCount =  $('#intesting > div').length;
	var inprogressdivCount =  $('#inprogress > div').length;
	var finisheddivCount =  $('#finished > div').length;
	document.getElementById("todonumber").innerHTML = tododivCount;
	document.getElementById("plannednumber").innerHTML = planeddivCount;
	document.getElementById("intestingnumber").innerHTML = testingdivCount;
	document.getElementById("inprogressnumber").innerHTML = inprogressdivCount;
	document.getElementById("finishednumber").innerHTML = finisheddivCount;
}