/**
 * Handle work by person, person and upload file button in user's home page
 */
		function callworkbyperson() {
			$.get( "workbyperson?viewWorkByPerson", function( data ) {
				$( "#mainContainer" ).html( data );
				
			});
		}
		function callperson() {
			
			$.get( "person?viewPerson", function( data ) {
				$( "#mainContainer" ).html( data );
				
			});
			
		}
		function callImageView() {
			$.get( "imageUpload?fileview", function( data ) {
				$( "#mainContainer" ).html( data );
				
			});
		}